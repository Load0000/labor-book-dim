package com.gitlab.tcsnzh.laborbook.pdfutil;

public record TextPosResult(String text, float x, float y) {
}
