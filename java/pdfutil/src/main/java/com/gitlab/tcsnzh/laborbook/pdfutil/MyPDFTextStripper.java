package com.gitlab.tcsnzh.laborbook.pdfutil;

import org.apache.pdfbox.contentstream.PDFStreamEngine;
import org.apache.pdfbox.contentstream.operator.Operator;
import org.apache.pdfbox.contentstream.operator.OperatorProcessor;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.TextPosition;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public class MyPDFTextStripper extends PDFTextStripper {
    public final ArrayList<MyTextInfo> resultList = new ArrayList<>(8192);

    @Override
    public void processPage(PDPage page) throws IOException {
//        System.out.println("============= Page " + getCurrentPageNo() + " =============");
        super.processPage(page);
    }

    protected volatile Map<String, OperatorProcessor> operators2;

    protected boolean beginText = false;
    protected final AtomicInteger beginTextCount = new AtomicInteger(0);

    @Override
    protected void processOperator(Operator operator, List<COSBase> operands) throws IOException {
        if (operators2 == null) {
            synchronized (this) {
                if (operators2 == null) {
                    try {
                        Field field_operators = PDFStreamEngine.class.getDeclaredField("operators");
                        field_operators.setAccessible(true);
                        //noinspection unchecked
                        operators2 = (Map<String, OperatorProcessor>) field_operators.get(this);
                    } catch (IllegalAccessException | NoSuchFieldException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }


        String opname = operator.getName();
        if ("BT".equals(opname)) {
            beginText = true;
        }
        if ("ET".equals(opname)) {
            beginText = false;
            resultList.add(new MyTextInfo.Split(beginTextCount.getAndIncrement()));
        }


        var op = Optional.ofNullable(operators2.get(opname))
                .map(it -> opname + "(" + it.getClass().getSimpleName() + ")")
                .orElse(opname);

//        System.out.printf("processOperator      : %s     %s %n", op, operands);
        super.processOperator(operator, operands);
    }

    @Override
    protected void processTextPosition(TextPosition tp) {
//        System.out.println("processTextPosition  : " + tp + "\t" + tp.getY());
        resultList.add(new MyTextInfo.TextPos(getCurrentPageNo(), tp.getUnicode(), tp.getX(), tp.getY(), tp.getPageWidth(), tp.getPageHeight()));
        super.processTextPosition(tp);
    }
}
