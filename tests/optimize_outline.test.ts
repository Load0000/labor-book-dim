import { expect, test } from "vitest";
import {
  optimize_outline,
  filepath_to_prefix_number_info,
} from "@labor-book/optimize_outline/src/index";
import {
  ignore_suffix_zero,
  parse_prefix_number,
} from "packages/utils/src/parse_prefix_number";

test("filepath_to_prefix_number_info", () => {
  expect(
    filepath_to_prefix_number_info("./book/health58/02/02_01_00.md")
  ).toEqual({
    filename: "02_01_00.md",
    suffix_idx: 8,
    prefix_numbers: [2, 1, 0],
  });
});

type N = {
  text: string;
  is_leaf?: boolean;
  children: N[];
};

async function test_optimize_outline_simple(
  testname: string,
  nodes: N[],
  expect_res: N[]
) {
  test(`optimize_outline_simple__${testname}`, async () => {
    const actual_res = await optimize_outline({
      nodes,
      items_flag: "children",
      hooks: {
        allow_replace_parent___on_shorten_single_branch_treenode: async (p) =>
          p.is_leaf != true,
        // allow_replace_parent___on_replace_when_the_child_node_prefix_number_are_samed:
        //   async (p) => p.is_leaf != true,
        // insert_to_original_position_of_child___on_replace_when_the_child_node_prefix_number_are_samed:
        //   async () => "concat",
        before_replace_to: async () => {},
        before_travel_node: async () => {},
        // prefix_number: async (n) => {
        //   return parse_prefix_number(n.text, ["_"], (it) => it).prefix_numbers;
        // },
      },
    });
    expect(actual_res).toEqual(expect_res);
  });
}

// await test_optimize_outline_simple(
//   "test_1",
//   [
//     {
//       text: "1_0",
//       children: [
//         {
//           text: "1_0_1",
//           children: [],
//         },
//       ],
//     },
//   ],
//   [
//     {
//       text: "1_0_1",
//       children: [],
//     },
//   ]
// );

// await test_optimize_outline_simple(
//   "test_2",
//   [
//     {
//       text: "1_0",
//       children: [
//         {
//           text: "1_0_1",
//           children: [],
//         },
//         {
//           text: "1_0_2",
//           children: [],
//         },
//       ],
//     },
//   ],
//   [
//     {
//       text: "1_0",
//       children: [
//         {
//           text: "1_0_1",
//           children: [],
//         },
//         {
//           text: "1_0_2",
//           children: [],
//         },
//       ],
//     },
//   ]
// );

// await test_optimize_outline_simple(
//   "test_3",
//   [
//     {
//       text: "1_0",
//       children: [
//         {
//           text: "1_0_0",
//           children: [],
//         },
//         {
//           text: "1_0_1",
//           children: [],
//         },
//         {
//           text: "1_0_2",
//           children: [],
//         },
//       ],
//     },
//   ],
//   [
//     {
//       text: "1_0_0",
//       children: [
//         {
//           text: "1_0_1",
//           children: [],
//         },
//         {
//           text: "1_0_2",
//           children: [],
//         },
//       ],
//     },
//   ]
// );

// await test_optimize_outline_simple(
//   "test_4",
//   [
//     {
//       text: "01_00_00.md",
//       children: [
//         {
//           text: "01_00_00.md",
//           children: [
//             {
//               text: "01_00_00.md",
//               children: [
//                 {
//                   text: "01_00_00.md",
//                   children: [
//                     {
//                       text: "01_00_00_急救手册使用须知.md",
//                       is_leaf: true,
//                       children: [],
//                     },
//                     {
//                       children: [],
//                       is_leaf: true,
//                       text: "01_01_01_本篇的目的.md",
//                     },
//                   ],
//                 },
//               ],
//             },
//           ],
//         },
//       ],
//     },
//     {
//       text: "02.md",
//       is_leaf: true,
//       children: [],
//     },
//   ],
//   [
//     {
//       text: "01_00_00_急救手册使用须知.md",
//       is_leaf: true,
//       children: [
//         {
//           children: [],
//           is_leaf: true,
//           text: "01_01_01_本篇的目的.md",
//         },
//       ],
//     },
//     {
//       text: "02.md",
//       is_leaf: true,
//       children: [],
//     },
//   ]
// );

// await test_optimize_outline_simple(
//   "test_5",
//   [
//     {
//       text: "01_00_00.md",
//       children: [
//         {
//           text: "01_00_00_急救手册使用须知.md",
//           is_leaf: true,
//           children: [],
//         },
//         {
//           children: [],
//           text: "01_01_01_本篇的目的.md",
//           is_leaf: true,
//         },
//       ],
//     },
//     {
//       text: "02.md",
//       children: [
//         {
//           text: "02_00_00_晕倒（意识丧失）.md",
//           is_leaf: true,
//           children: [],
//         },
//         {
//           text: "02_01_00_如何快速识别晕倒工友的症状？.md",
//           is_leaf: true,
//           children: [],
//         },
//       ],
//     },
//   ],
//   [
//     {
//       text: "01_00_00_急救手册使用须知.md",
//       is_leaf: true,
//       children: [
//         { text: "01_01_01_本篇的目的.md", is_leaf: true, children: [] },
//       ],
//     },
//     {
//       text: "02_00_00_晕倒（意识丧失）.md",
//       is_leaf: true,
//       children: [
//         {
//           text: "02_01_00_如何快速识别晕倒工友的症状？.md",
//           is_leaf: true,
//           children: [],
//         },
//       ],
//     },
//   ]
// );
