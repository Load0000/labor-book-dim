// import { expect } from "vitest";
// import { MainContext, _all_log_level } from "../gen_pdf/src/main.node";
// import package_json from "../package.json";

// export function create_test_main_context(
//   test_name: string,
//   booknames: readonly string[]
// ) {
//   return new MainContext({
//     enable_log_level: [..._all_log_level].filter((it) => it != "debug"),
//     docs_dir: "./book_test_template",
//     dist_dir: `./tests/dist/${test_name}/`,
//     cache_dir: `./tests/.cache/${test_name}/`,
//     cache_index_file_relative_path: "output_index.json",
//     styles_dir: "./gen_pdf/styles",
//     booknames,
//     preview_server: {
//       port: package_json.config.pdf.previewserver.port,
//     },
//     is_test: true,
//   });
// }

// export function _check_tree_nodes<
//   Children extends keyof N,
//   N extends Record<Children, N[] | undefined>
// >(
//   children_property_name: Children,
//   nodes_actual: N[],
//   nodes_expect: {
//     [key in keyof N]: N[key];
//   }[]
// ) {
//   const stack_info = (obj: any) =>
//     `stack info were ${JSON.stringify(obj, null, 2)} .`;
//   expect(
//     nodes_actual.length,
//     `expect length should greater then actual !${stack_info({
//       nodes_actual,
//       nodes_expect,
//     })}`
//   ).greaterThanOrEqual(nodes_expect.length);
//   for (let i = 0; i < nodes_expect.length; i++) {
//     const node_expect = nodes_expect[i];
//     const node_actual = nodes_actual[i];
//     const keys_expect = Object.keys(node_expect);
//     for (const key_expect of keys_expect) {
//       if (key_expect == children_property_name) {
//         continue;
//       }
//       const value_expect = node_expect[key_expect];
//       if (value_expect == undefined) {
//         continue;
//       }
//       const value_actual = node_actual[key_expect];
//       expect(
//         value_actual,
//         `key value should match !${stack_info({
//           key_expect,
//           value_actual,
//           value_expect,
//           i,
//           children_property_name,
//           node_actual,
//           node_expect,
//           nodes_actual,
//           nodes_expect,
//         })}`
//       ).eq(value_expect);
//     }
//     const children_expect = node_expect[children_property_name];
//     if (children_expect == undefined) {
//       continue;
//     }
//     const children_actual = node_actual[children_property_name];
//     if (children_actual == undefined) {
//       throw new Error(
//         `Require children_actual not undefined ! ${stack_info({
//           children_property_name,
//           children_actual,
//           children_expect,
//           i,
//           node_actual,
//           node_expect,
//           nodes_actual,
//           nodes_expect,
//         })}`
//       );
//     }
//     _check_tree_nodes(children_property_name, children_actual, children_expect);
//   }
// }
