import { expect, test } from "vitest";
import {
  parse_prefix_number,
  ignore_suffix_zero,
} from "@labor-book/utils/src/parse_prefix_number";

test("parse_prefix_number", () => {
  expect(parse_prefix_number("1_2_3_aaa.md", ["_"], (it) => it)).toEqual({
    suffix_idx: 6,
    prefix_numbers: [1, 2, 3],
  });
});

test("ignore_suffix_zero", () => {
  expect(ignore_suffix_zero([1, 2, 4, 0])).toEqual([1, 2, 4]);
});
