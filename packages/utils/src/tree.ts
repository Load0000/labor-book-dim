export type TreeNode<ItemsFlag extends string, SUBTYPE> = Record<
  ItemsFlag,
  SUBTYPE[]
>;

export function map_tree<
  ItemsFlag extends string,
  N extends TreeNode<ItemsFlag, N>,
  R extends TreeNode<ItemsFlag, R>
>(_arg: {
  node: N;
  items_flag: ItemsFlag;
  fn: (node: N) => Omit<R, ItemsFlag>;
}): R {
  const { node, items_flag, fn } = _arg;
  return {
    ...fn(node),
    [items_flag]: node[items_flag].map((it) =>
      map_tree({ node: it, items_flag, fn })
    ),
  } as R; // TODO: not use `as R`
}
