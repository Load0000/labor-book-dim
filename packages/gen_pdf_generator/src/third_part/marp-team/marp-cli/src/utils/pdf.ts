//
// This file is modified from :
// https://github.com/marp-team/marp-cli/blob/main/src/utils/pdf.ts
//
// Please see the LICENSE file
//
import { OutlineNode } from "../../../../../indexing_pdf.node";
import pdflib, {
  PDFArray,
  PDFContext,
  PDFDict,
  PDFDocument,
  PDFHexString,
  PDFName,
  PDFNumber,
  PDFObject,
  PDFRef,
  PDFString,
} from "pdf-lib";

// --- Outline ---

export type PDFOutlineTo = number | { page: number; x: number; y: number };

export interface PDFOutlineItem {
  title: string;
  to: PDFOutlineTo;
  italic?: boolean;
  bold?: boolean;
}

export interface PDFOutlineItemWithChildren extends Omit<PDFOutlineItem, "to"> {
  to?: PDFOutlineTo;
  children: PDFOutline[];
  open: boolean;
}

export type PDFOutline = PDFOutlineItem | PDFOutlineItemWithChildren;

/**
 * TABLE 8.3 Entries in the outline dictionary
 *
 */
export type EntriesInTheOutlineDictionary = {
  Type: "Outlines";
  Count: number;
  First?: EntriesInAnOutlineItemDictionary;
  Last?: EntriesInAnOutlineItemDictionary;
};

/**
 * TABLE 8.4 Entries in an outline item dictionary
 */
export type EntriesInAnOutlineItemDictionary = PDFRef;

function walk<
  T extends
    | {
        children: readonly T[];
      }
    | {}
>(
  items: readonly T[],
  callback: (it: T) => void | boolean // stop walking to children if returned false
) {
  for (const item of items) {
    const ret = callback(item);
    if (ret !== false && "children" in item && item.children != undefined) {
      walk(item.children, callback);
    }
  }
}

function flatten<
  T extends
    | {
        children: readonly T[];
      }
    | {}
>(outlines: readonly T[]) {
  const result: T[] = [];

  walk(outlines, (outline) => void result.push(outline));

  // console.log(`
  // Flatten :

  // ${JSON.stringify(result, null, 2)}

  // `);

  return result;
}

const getOpeningCount = (outlines: readonly PDFOutline[]) => {
  let count = 0;

  walk(outlines, (outline) => {
    count += 1;
    return !("open" in outline && !outline.open);
  });

  return count;
};

function _get_page_refs(doc: PDFDocument): PDFRef[] {
  const refs = [] as ReturnType<typeof _get_page_refs>;

  doc.catalog.Pages().traverse((kid, ref) => {
    if (kid.get(kid.context.obj("Type"))?.toString() === "/Page") {
      refs.push(ref);
    }
  });

  return refs;
}

// Outlines
function create_outline(_obj: {
  doc: PDFDocument;
  outlines: readonly PDFOutline[];
  parent: PDFRef;
  ctx?: {
    page_refs: PDFRef[];
    outline_to_ref: Map<PDFOutline, PDFRef>;
  };
}) {
  const { doc, outlines, parent } = _obj;

  const ctx = (() => {
    if (_obj.ctx != null) {
      return _obj.ctx;
    }

    const outline_to_ref = new Map<PDFOutline, PDFRef>();

    for (const outline of flatten(outlines)) {
      outline_to_ref.set(outline, doc.context.nextRef());
    }

    return {
      page_refs: _get_page_refs(doc),
      outline_to_ref,
    };
  })();

  const { page_refs, outline_to_ref } = ctx;

  for (let i = 0; i < outlines.length; i += 1) {
    const outline = outlines[i];
    const outlineRef = outline_to_ref.get(outline)!;

    const destOrAction = (() => {
      if (outline.to === undefined) {
        return {};
      } else if (typeof outline.to === "number") {
        return { Dest: [page_refs[outline.to], "Fit"] };
      } else {
        return {
          Dest: [
            page_refs[outline.to.page],
            "XYZ",
            outline.to.x,
            outline.to.y,
            null,
          ],
        };
      }
    })();

    const childrenDict: EntriesInTheOutlineDictionary | {} = (() => {
      if ("children" in outline && outline.children.length > 0) {
        create_outline({
          doc,
          outlines: outline.children,
          parent: outlineRef,
          ctx,
        });
        return {
          First: outline_to_ref.get(outline.children[0])!,
          Last: outline_to_ref.get(
            outline.children[outline.children.length - 1]
          )!,
          Count: getOpeningCount(outline.children) * (outline.open ? 1 : -1),
        };
      }
      return {};
    })();

    doc.context.assign(
      outlineRef,
      doc.context.obj({
        Title: PDFHexString.fromText(outline.title),
        Parent: parent,
        ...(i > 0 ? { Prev: outline_to_ref.get(outlines[i - 1])! } : {}),
        ...(i < outlines.length - 1
          ? { Next: outline_to_ref.get(outlines[i + 1])! }
          : {}),
        ...childrenDict,
        ...destOrAction,
        F: (outline.italic ? 1 : 0) | (outline.bold ? 2 : 0),
      })
    );
  }

  return ctx;
}

function _assign_ref_to_obj__and__set_ref_by_key_on_catalog(_obj: {
  doc: PDFDocument;
  ref: PDFRef;
  obj:
    | EntriesInTheOutlineDictionary
    | {
        Type: "Destinations";
        [k: string]: any;
      };
}) {
  const { doc, ref, obj } = _obj;
  doc.context.assign(ref, doc.context.obj(obj));
  doc.catalog.set(doc.context.obj(obj.Type), ref);
}

export type OutlineNodeWithDisplayTitle = Omit<
  OutlineNode<number>,
  "children"
> & {
  display_title: string;
  children: OutlineNodeWithDisplayTitle[];
};

export async function setOutline(
  doc: PDFDocument,
  outlines: readonly PDFOutline[]
) {
  // Outline

  const outline_root_ref = doc.context.nextRef();

  const { outline_to_ref } = create_outline({
    doc,
    outlines,
    parent: outline_root_ref,
  });

  const outlineCount = getOpeningCount(outlines);

  _assign_ref_to_obj__and__set_ref_by_key_on_catalog({
    doc,
    ref: outline_root_ref,
    obj: {
      // Type: "Destinations",
      Type: "Outlines",
      ...(outlineCount > 0
        ? {
            First: outline_to_ref.get(outlines[0])!,
            Last: outline_to_ref.get(outlines[outlines.length - 1])!,
          }
        : {}),
      Count: outlineCount,
    },
  });

  // Destiantion

  // const destinations_ref = doc.context.nextRef();

  // const destinations = doc.context.obj(["Doc-Start", "XYZ", 0, 0, 0]);

  // doc.catalog.set(PDFName.of("Destinations"), destinations_ref);

  // doc.context.assign(destinations_ref, destinations);

  // _assign_ref_to_obj__and__set_ref_by_key_on_catalog({
  //   doc,
  //   ref: destinations_ref,
  //   obj: {
  //     Type: "Destinations",
  //   },
  // });
}
