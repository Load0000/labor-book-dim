import child_process, { ChildProcess } from "node:child_process";
import process from "node:process";
import pstree, { PS } from "ps-tree";
import is_running from "is-running";
import fs from "fs/promises";
import { _replace_suffix } from "@labor-book/utils/src/util";
import { logger } from "@labor-book/utils/src/log";

export async function _kill_proc_recursion(
  proc: PS,
  parent_is_alive: () => boolean
) {
  logger.trace({ proc }, "Process group killing start");
  const me_pid = Number.parseInt(proc.PID);
  const children = await new Promise<readonly PS[]>((resolve, reject) => {
    pstree(me_pid, (err, children) => {
      if (err) reject(err);
      else resolve(children);
    });
  });
  logger.trace({ proc, children }, "Process children finded");
  for (const child of children) {
    if (!parent_is_alive()) {
      return;
    }
    await _kill_proc_recursion(child, function () {
      if (!parent_is_alive()) {
        return false;
      }
      return is_running(me_pid);
    });
  }
  try {
    if (!parent_is_alive()) {
      return;
    }
    if (is_running(me_pid)) {
      logger.trace({ proc }, "Process self killing start");
      process.kill(me_pid);
    } else {
      logger.trace({ proc }, "Process self already died");
    }
  } catch (err) {
    logger.warn({ proc, err }, `"Error on process killing "`);
    throw err;
  }
}

export async function _kill_child_subprocess(
  proc_nickname: string,
  proc: child_process.ChildProcessByStdio<any, any, any>
) {
  if (!proc.killed) {
    const child_children = await new Promise<readonly PS[]>(
      (resolve, reject) => {
        pstree(proc.pid!!, (err, children) => {
          if (err) reject(err);
          else resolve(children);
        });
      }
    );
    logger.trace(
      { proc_nickname, child_children },
      "Readed pstree result , start group killing"
    );
    for (const child_child of child_children) {
      logger.trace(
        {
          proc_nickname,
          COMMAND: child_child.COMMAND,
        },
        "killing group"
      );
      await _kill_proc_recursion(child_child, () => !proc.killed);
    }
    logger.trace({ proc_nickname }, "success group killed");
    if (proc.killed) {
      logger.trace({ proc_nickname }, "Parent process killing");
      while (!proc.kill("SIGINT")) {
        logger.warn({ proc_nickname }, "Parent process killing failed");
      }
      logger.trace({ proc_nickname }, `Process killed successed`);
    }
  } else {
    logger.trace(
      { proc_nickname },
      `Process already died , maybe it stop when children died`
    );
  }
}

export async function _exist_pth(pth: string): Promise<boolean> {
  try {
    await fs.access(pth, fs.constants.R_OK | fs.constants.W_OK);
    return true;
  } catch (err) {
    return false;
  }
}

export function _sub_proc_pipe2std_and_await(proc: ChildProcess) {
  proc.stdout?.pipe(process.stdout);
  proc.stderr?.pipe(process.stderr);
  return new Promise((resolve, reject) => {
    proc.on("exit", () => {
      if (proc.exitCode !== 0) {
        reject(new Error("command not return zero"));
      } else {
        resolve(proc);
      }
    });
  });
}
