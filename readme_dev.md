# 开发者文档

## 源神，启动！

最少需要:

- Node.js 版本 18 或 更高.
- 一个支持 Markdown 格式的文本编辑器.
  - 推荐使用 VSCode ， 因为它还能写 Vue 组件 .

```shell
npm i --loglevel verbose
```

如果你打算开发 pdf 生成器。你还需要:

- jdk>=17
- Chrome or Chromium

## 排错

### Windows Powershell Vscode Terminal 中文乱码

原因: pino 输出编码为 utf-8 的日志文本到 windows gbk 控制台。

解决方法:

```powershell
chcp 65001
```

### Unknown file extension ".ts"

`npm run dev` 时报错:

```
TypeError [ERR_UNKNOWN_FILE_EXTENSION]: Unknown file extension ".ts"
```

解决方法:

https://github.com/vitejs/vite/issues/5370#issuecomment-1339022262

### 关于 MPA

MPA 下问题很多，比如侧边栏和顶栏按钮完全无反应，一些组件（如 JumpBox ）也出问题。

尚待修复。

> 现在仅仅在生成 pdf 时使用 MPA 模式。毕竟靠 puppeteer 打印 pdf 不需要与 javascript 有过多交互。