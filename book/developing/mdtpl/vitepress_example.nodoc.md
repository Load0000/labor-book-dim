# VitePress 的 Markdown 扩展

## 自定义框框

:::info 自定义框框
这是一个普普通通的自定义框框
:::

:::tip 自定义框框2
这也是一个代表着“强调内容”自定义框框
:::

:::warning 自定义框框3
这也是一个代表着“警告”自定义框框
:::

:::danger 自定义框框4
这也是一个代表着“危险”自定义框框
:::

:::details 自定义框框5
这也是一个可以折叠大段信息的自定义框框
:::

## 包含其他 Markdown 文件

参考: https://vitepress.dev/zh/guide/markdown#markdown-file-inclusion

包含别的 Markdown 文件的内容：

<!--@include: ./a_short_text.nodoc.md-->

## 标签页代码框

标签页代码框:

:::code-group

```md [你好]
你好呀
```

```md [世界]
世界和平
```

:::
