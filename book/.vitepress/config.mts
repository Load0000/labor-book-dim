import { defineConfig } from "vitepress";
import { _common_sidebar } from "./sidebar.node.js";
import { _require_home_action, get_home_info } from "./home.node.ts";
import package_json from "../../package.json";
import fs from "fs/promises";
import { _replace_suffix } from "../../packages/utils/src/util.ts";
import { logger } from "../../packages/utils/src/log.ts";

const { home_title, home_description, home_actions } = await get_home_info({
  docs_dir_suffixed: "./book/",
});
const health58 = _require_home_action("health58", home_actions);
const exercise58 = _require_home_action("exercise58", home_actions);

//
// https://vitepress.dev/reference/site-config
//
// Run in Node.js env
//
export default defineConfig({
  lang: "zh-CN",
  title: home_title,
  description: home_description,
  vite: {
    configFile: "./vite.config.ts",
  },
  head: [
    ['link', { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    // ['link', { rel: 'icon', type: 'image/svg+xml', href: '/path/to/icon.svg' }], for svg
  ],
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: "主页", link: "/" },
      {
        text: health58.text,
        link: health58.link,
      },
    ],

    lastUpdated: {
      text: "本章节最后更新于 ",
      formatOptions: {
        dateStyle: "full",
        timeStyle: "medium",
      },
    },

    editLink: {
      pattern: (pagedata) => {
        return `/developing/01_00_00.html?page_title=${encodeURIComponent(
          pagedata.title
        )}&page_filepath=${encodeURIComponent(pagedata.filePath)}`;
      },
      text: "编辑或提出意见",
    },
    darkModeSwitchLabel: "暗色模式",
    sidebarMenuLabel: "全部目录",
    returnToTopLabel: "回到网页顶部",
    langMenuLabel: "语言",
    notFound: {
      title: "页面不见了",
      quote: "或许是文件发生了更改而导致链接失效...",
      linkText: "返回首页",
    },
    docFooter: {
      next: "下一章",
      prev: "上一章",
    },
    outline: {
      label: "跳转标题或顶部",
      level: "deep",
    },
    sidebar: [
      {
        text: "添砖加瓦",
        collapsed: true,
        items: await _common_sidebar({
          docs_dir_suffixed: "./book/",
          book_id: "developing",
        }),
      },
      {
        text: health58.text,
        collapsed: true,
        items: await _common_sidebar({
          docs_dir_suffixed: "./book/",
          book_id: "health58",
        }),
      },
      {
        text: exercise58.text,
        collapsed: true,
        items: await _common_sidebar({
          docs_dir_suffixed: "./book/",
          book_id: "exercise58",
        }),
      },
    ],

    socialLinks: [
      {
        icon: {
          svg: await fs.readFile("./book/.vitepress/assets/git.svg", {
            encoding: "utf-8",
          }),
        },
        link: package_json.repository.url,
        ariaLabel: "Git",
      },
    ],
  },
});
