import { fileURLToPath } from "node:url";
import { defineConfig } from "vite";
import childProcess from "child_process";

export default defineConfig(({ command, mode, isSsrBuild, isPreview }) => {
  return {
    resolve: {
      alias: {
        "@utils": fileURLToPath(
          new URL("./packages/utils/src", import.meta.url)
        ),
      },
    },
    define: {
      APP_GIT_VERSION_COUNT: JSON.stringify(
        childProcess.execSync("git rev-list HEAD --count").toString().trim()
      ),
      APP_GIT_NAME_REV: JSON.stringify(
        childProcess.execSync("git name-rev --name-only HEAD").toString().trim()
      ),
      APP_GIT_HEAD: JSON.stringify(
        childProcess.execSync("git rev-parse HEAD").toString().trim()
      ),
      APP_GIT_DIRTY: JSON.stringify(
        childProcess.execSync("git status -s -uall").toString().split("\n")
      ),
      APP_BUILD_TIME: JSON.stringify(
        new Date().toLocaleString("zh-CN", {
          timeZone: "Asia/ShangHai",
          timeZoneName: "long",
        })
      ),
    },
  };
});
